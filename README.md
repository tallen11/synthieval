# README #

Source code for a retro, ASCII style, roguelike game that I'm working on in my spare time. Written in Java. I'm an aspiring programmer/developer, so feel free to point out any mistakes that this source is inevitably riddled with :)

### Note ###
AsciiPanel.jar was not created by me and all credit goes to its original creator here: [AsciiPanel](https://github.com/trystan/AsciiPanel/tree/master/src/asciiPanel)