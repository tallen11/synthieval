package com.tateallen.syntheival.displays;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

import com.tateallen.syntheival.containment.Display;
import com.tateallen.syntheival.helpers.Manager;

public class DisplayHome extends Display {

	@Override
	public void draw(AsciiPanel terminal) {
		super.draw(terminal);

		terminal.writeCenter("Synthieval Alpha", 12);
		terminal.writeCenter("[SPACE]", 13);
	}

	@Override
	public void keyPressed(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.VK_SPACE)
			Manager.getManager().changeActiveDisplay(new DisplayTesting());
	}

	@Override
	public void update() { }
}
