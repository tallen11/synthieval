package com.tateallen.syntheival.displays;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

import com.tateallen.syntheival.containment.DialogView;
import com.tateallen.syntheival.containment.Display;
import com.tateallen.syntheival.containment.GameView;
import com.tateallen.syntheival.containment.InventoryView;
import com.tateallen.syntheival.entities.Player;
import com.tateallen.syntheival.helpers.Manager;
import com.tateallen.syntheival.worldinfo.GameWorld;
import com.tateallen.syntheival.worldinfo.generators.CavesGenerator;
import com.tateallen.syntheival.worldinfo.generators.PlanetGenerator;

public class DisplayTesting extends Display {
	
	private GameWorld theWorld;
	private Player thePlayer;
	private final int PORT_WIDTH = 32;
	private final int PORT_HEIGHT = 20;
	private final int PORT_OFF_X = 1;
	private final int PORT_OFF_Y = 2;
	
	private GameView mainWorldView;
	private InventoryView invView;
	private DialogView dialogView;
		
	public DisplayTesting() {
		theWorld = CavesGenerator.generateCaves2(200, 200);
		//theWorld = PlanetGenerator.generateTestRegionedWorld(100, 100);
		
		thePlayer = new Player(theWorld);
		thePlayer.moveTo(2, 2);
		
		mainWorldView = new GameView(theWorld, thePlayer);
		mainWorldView.setOriginX(PORT_OFF_X);
		mainWorldView.setOriginY(PORT_OFF_Y);
		mainWorldView.setWidth(Manager.getManager().getScreenWidth() - PORT_WIDTH/2 - 5);
		mainWorldView.setHeight(PORT_HEIGHT);
		mainWorldView.setBorder(true);
		addNode(mainWorldView);
		
		invView = new InventoryView(thePlayer);
		invView.setOriginX(Manager.getManager().getScreenWidth() - PORT_WIDTH/2 - 2);
		invView.setOriginY(PORT_OFF_Y);
		invView.setWidth(PORT_WIDTH/2);
		invView.setHeight(PORT_HEIGHT);
		invView.setBorder(true);
		addNode(invView);
		
		dialogView = new DialogView();
		dialogView.setOriginX(1);
		dialogView.setOriginY(PORT_OFF_Y + mainWorldView.getHeight());
		dialogView.setWidth(Manager.getManager().getScreenWidth() - 3);
		dialogView.setHeight(Manager.getManager().getScreenHeight() - dialogView.getOriginY() - 1);
		dialogView.generateLabels();
		addNode(dialogView);
		
		if (theWorld.getPlayerSpawnLocation() != null) {
			thePlayer.moveTo(theWorld.getPlayerSpawnLocation().x, theWorld.getPlayerSpawnLocation().y);
		}
	}

	@Override
	public void draw(AsciiPanel terminal) {
		super.draw(terminal);
		
		//Clear top bar
		for (int i = 0; i < 80; i++)
			terminal.write(' ', i, 0);
		
		//Print coordinates and player glyph
		terminal.writeCenter("X:" + thePlayer.getX() + " Y:" + thePlayer.getY(), 0);
		terminal.write(thePlayer.getGlyph(), mainWorldView.getWidth()/2+PORT_OFF_X, mainWorldView.getHeight()/2+PORT_OFF_Y, thePlayer.getColor());
	}

	@Override
	public void keyPressed(KeyEvent event) {
		final int moveSpeed = 1;
		switch (event.getKeyCode()) {
		case KeyEvent.VK_UP:
			if (theWorld.canMoveBy(thePlayer, 0, -1))
				thePlayer.moveBy(0, -moveSpeed);
			break;
			
		case KeyEvent.VK_DOWN:
			if (theWorld.canMoveBy(thePlayer, 0, 1))
				thePlayer.moveBy(0, moveSpeed);
			break;
			
		case KeyEvent.VK_LEFT:
			if (theWorld.canMoveBy(thePlayer, -1, 0))
				thePlayer.moveBy(-moveSpeed, 0);
			break;
			
		case KeyEvent.VK_RIGHT:
			if (theWorld.canMoveBy(thePlayer, 1, 0))
				thePlayer.moveBy(moveSpeed, 0);
			break;
		}
	}

	@Override
	public void update() { }
}
