package com.tateallen.syntheival.items;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.helpers.DialogueManager;
import com.tateallen.syntheival.iteminfo.ItemWeapon;

public class IronSword extends ItemWeapon {

	public IronSword() {
		super("Iron Sword", 12);
	}
	
	@Override
	public void useItem(Entity usingEntity) {
		DialogueManager.getManager().displayMessage("A sword");
	}
}
