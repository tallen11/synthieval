package com.tateallen.syntheival.items;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.helpers.DialogueManager;
import com.tateallen.syntheival.iteminfo.ItemWeapon;

public class WoodenSword extends ItemWeapon {

	public WoodenSword() {
		super("Wooden Sword", 4);
	}
	
	@Override
	public void useItem(Entity usingEntity) {
		DialogueManager.getManager().displayMessage("You look down at your sword and see that it's made of wood... and it's stupid.");
	}
}
