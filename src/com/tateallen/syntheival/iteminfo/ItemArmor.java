package com.tateallen.syntheival.iteminfo;

public class ItemArmor extends Item {

	private int damageAbsorbtion;
	
	public ItemArmor(String name, int damageAbsorbtion) {
		super(name);
		this.damageAbsorbtion = damageAbsorbtion;
	}
	
	public int getDamageAbsorbtion() {
		return damageAbsorbtion;
	}
	
	public int getUnabsorbedDamage(int damage) {
		return damage - (damage * (getDamageAbsorbtion() / 100));
	}
}
