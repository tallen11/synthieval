package com.tateallen.syntheival.iteminfo;

import com.tateallen.syntheival.entityinfo.Entity;

public class Item {
	
	private String name;
	
	public Item(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void useItem(Entity usingEntity) { };
	public void useItemOn(Entity usingEntity, Entity victim) { };
	
	@Override
	public String toString() {
		return name;
	}
}
