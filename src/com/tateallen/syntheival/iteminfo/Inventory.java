package com.tateallen.syntheival.iteminfo;

import java.util.ArrayList;
import java.util.Iterator;

public class Inventory implements Iterable<Item> {
	
	public enum AddItemAttempt {
		SUCCESS,
		FAILURE_FULL
	}

	private ArrayList<Item> inventoryData;
	private int maxInventorySize;
	
	public Inventory(int maxInventorySize) {
		this.inventoryData = new ArrayList<Item>();
		this.maxInventorySize = maxInventorySize;
	}
	
	public Inventory(int maxInventorySize, Item...startingItems) {
		this.inventoryData = new ArrayList<Item>();
		this.maxInventorySize = maxInventorySize;
		for (Item item : startingItems) {
			if (addItem(item) == AddItemAttempt.FAILURE_FULL) {
				System.out.println("Cannot start with that many items!");
			}
		}
	}
	
	public int getInventorySize() {
		return inventoryData.size();
	}
	
	public AddItemAttempt addItem(Item item) {
		if (!isFull()) {
			inventoryData.add(item);
			return AddItemAttempt.SUCCESS;
		}
		
		return AddItemAttempt.FAILURE_FULL;
	}
	
	public void transferInventoryIn(Inventory inv) {
		for (Item item : inv) {
			if (!isFull()) {
				addItem(item);
			}
		}
		
		inv.clearInventory();
	}
	
	public void dropItem(Item item) {
		inventoryData.remove(item);
	}
	
	public void clearInventory() {
		inventoryData.clear();
	}
	
	public boolean isFull() {
		return getInventorySize() >= maxInventorySize;
	}
	
	public boolean isEmpty() {
		return getInventorySize() == 0;
	}
	
	public void sortInventory() {
		//TODO: NYI
	}
	
	@Override
	public String toString() {
		//TODO: Possibly update
		return inventoryData.toString();
	}

	@Override
	public Iterator<Item> iterator() {
		return inventoryData.iterator();
	}
}
