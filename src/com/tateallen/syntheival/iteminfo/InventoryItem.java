package com.tateallen.syntheival.iteminfo;

public class InventoryItem<T> {
	
	private Class<T> itemClass;
	private String name;
	private int count;
	
	public InventoryItem(String name) {
		this.name = name;
		this.count = 1;
	}
	
	public void addItem() {
		count++;
	}
	
	public void removeItem() {
		count--;
	}
	
	public T getItem() {
		try {
			return itemClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj instanceof InventoryItem) {
			InventoryItem<?> item = (InventoryItem<?>)obj;
			return item.getName().equals(getName());
		}
		
		return false;
	}
}
