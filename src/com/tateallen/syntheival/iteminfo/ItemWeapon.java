package com.tateallen.syntheival.iteminfo;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.entityinfo.EntityLiving;

public class ItemWeapon extends Item {
	
	private int damageValue;

	public ItemWeapon(String name, int damageValue) {
		super(name);
		this.damageValue = damageValue;
	}
	
	public int getDamage() {
		return damageValue;
	}

	@Override
	public void useItemOn(Entity usingEntity, Entity victim) {
		if (victim.getClass().equals(EntityLiving.class)) {
			EntityLiving living = (EntityLiving)victim;
			living.damage(getDamage());
		} else {
			System.out.println("ERROR: Tried to attack non-living target!");
		}
	}
}
