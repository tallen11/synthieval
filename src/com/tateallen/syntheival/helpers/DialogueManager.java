package com.tateallen.syntheival.helpers;

import java.util.ArrayList;
import asciiPanel.AsciiPanel;
import com.tateallen.syntheival.containment.Label;

public class DialogueManager {

	private static DialogueManager instance = null;
	private ArrayList<String> messages;
	private ArrayList<Label> labels;
	
	private DialogueManager() {
		messages = new ArrayList<String>();
		labels = new ArrayList<Label>();
	}
	
	public static DialogueManager getManager() {
		if (instance == null)
			instance = new DialogueManager();
		return instance;
	}
	
	public void displayMessage(String message) {
		messages.add(message);
		
		for (int i = 0; i < labels.size(); i++) {
			Label lab = labels.get(i);
			if (i+1 < labels.size()) {
				Label nextLab = labels.get(i+1);
				lab.setText(nextLab.getText());
				lab.setColor(AsciiPanel.brightBlack);
			} else {
				lab.setColor(AsciiPanel.white);
				lab.setText(message);
			}
		}
	}
		
	public void registerLabel(Label label) {
		labels.add(label);
	}
	
	public void deregisterLabel(Label label) {
		labels.remove(label);
	}
}
