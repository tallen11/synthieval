package com.tateallen.syntheival.helpers;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.tateallen.syntheival.worldinfo.GameWorld;

public class MapRenderer {
	
	public static void renderWorldToFile(GameWorld world, String fileName) {
		int imageWidth = world.getWidth() * 8;
		int imageHeight = world.getHeight() * 16;
		BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g = image.getGraphics();
		g.setFont(new Font("Fixedsys", Font.PLAIN, 24));
		Graphics2D graphics = (Graphics2D)g;
 		
		for (int x = 0; x < world.getWidth(); x++) {
			for (int y = 0; y < world.getHeight(); y++) {
				graphics.drawString(""+world.getTileAt(x, y).getGlyph(), x*8, y*16);
			}
		}
		
		try {
			ImageIO.write(image, "png", new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
