package com.tateallen.syntheival.helpers;

public class Range {
	
	private int min;
	private int max;
	
	public Range(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
	
	public int getLength() {
		return max - min;
	}
	
	public boolean inRange(int N) {
		return N >= min && N <= max;
	}
}
