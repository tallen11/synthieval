package com.tateallen.syntheival.helpers;

import java.util.Random;

public class Utils {
	
	public static final Random RNG = new Random();
	
	public enum Direction {
		UP,
		DOWN,
		LEFT,
		RIGHT
	}
	
	public static boolean inBounds(int x, int y, int width, int height) {
		return x >= 0 && x < width && y >= 0 && y < height;
	}
	
	public static int convertToRange(int oldValue, int oldMin, int oldMax, int newMin, int newMax) {		
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	public static float distance(int x1, int y1, int x2, int y2) {
		return (float)Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}
	
	public static boolean inRange(int val, int low, int high) {
		return val >= low && val <= high;
	}
}
