package com.tateallen.syntheival.helpers;

import com.tateallen.syntheival.Synthieval;
import com.tateallen.syntheival.containment.Display;

public class Manager {
	
	private static Manager instance = null;
	private Synthieval gameInstance = null;
	private int screenWidth;
	private int screenHeight;
	
	private Manager() {
		gameInstance = null;
		screenWidth = -1;
		screenHeight = -1;
	}
	
	public static Manager getManager() {
		if (instance == null)
			instance = new Manager();
		return instance;
	}
	
	public void setGameInstance(Synthieval gameInstance) {
		this.gameInstance = gameInstance;
	}
	
	public void changeActiveDisplay(Display newDisplay) {
		gameInstance.changeActiveDisplay(newDisplay);
	}
	
	public void setScreenDimensions(int width, int height) {
		screenWidth = width;
		screenHeight = height;
	}
	
	public int getScreenWidth() {
		return screenWidth;
	}
	
	public int getScreenHeight() {
		return screenHeight;
	}
}
