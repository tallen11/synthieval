package com.tateallen.syntheival.entities;

import java.awt.Color;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.helpers.Manager;
import com.tateallen.syntheival.worldinfo.GameWorld;
import com.tateallen.syntheival.worldinfo.World;

public class Gateway extends Entity {
	
	private World transitionWorld;

	public Gateway(GameWorld world, char glyph, Color color, World transitionWorld) {
		super(world, glyph, color, EntityType.NON_LIVING);
		this.transitionWorld = transitionWorld;
	}
	
	@Override
	public void interactedWith(Entity entity) {
		//if (entity.getType() == EntityType.PLAYER)
		//	Manager.getManager().changeActiveDisplay(new DisplayWorld(transitionWorld, (Player)entity));
	}
}
