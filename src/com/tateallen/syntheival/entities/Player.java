package com.tateallen.syntheival.entities;

import asciiPanel.AsciiPanel;

import com.tateallen.syntheival.entityinfo.EntityLiving;
import com.tateallen.syntheival.entityinfo.FOV;
import com.tateallen.syntheival.worldinfo.GameWorld;

public class Player extends EntityLiving {

	public Player(GameWorld world) {
		super(world, (char)1, AsciiPanel.brightYellow, 10, EntityType.PLAYER, new FOV(10));
	}
	
	@Override
	public void moveTo(int x, int y) {
		this.xPos = x;
		this.yPos = y;
	}
}
