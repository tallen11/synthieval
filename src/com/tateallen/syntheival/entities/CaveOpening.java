package com.tateallen.syntheival.entities;

import java.awt.Color;

import com.tateallen.syntheival.worldinfo.GameWorld;
import com.tateallen.syntheival.worldinfo.World;

public class CaveOpening extends Gateway {
	
	public enum OpeningDirection {
		ENTRANCE,
		EXIT
	}

	public CaveOpening(GameWorld world, World transitionWorld, OpeningDirection openingDirection) {
		super(world, (char)9, Color.black, transitionWorld);
		if (openingDirection == OpeningDirection.EXIT)
			setColor(new Color(0, 192, 235));
	}
}
