package com.tateallen.syntheival.entities;

import java.awt.Color;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.entityinfo.EntityLiving;
import com.tateallen.syntheival.helpers.DialogueManager;
import com.tateallen.syntheival.iteminfo.Inventory;
import com.tateallen.syntheival.iteminfo.Item;
import com.tateallen.syntheival.worldinfo.GameWorld;

public class Chest extends Entity {
	
	//TODO: Create container class to encompass everything and future classes
	
	private Inventory inventory;
	
	public Chest(GameWorld world) {
		super(world, (char)10, new Color(166, 110, 22), EntityType.NON_LIVING);
		inventory = new Inventory(100);
	}
	
	public Chest(GameWorld world, Item...containedItems) {
		super(world, (char)10, new Color(166, 110, 22), EntityType.NON_LIVING);
		inventory = new Inventory(100, containedItems);
	}
	
	@Override
	public void interactedWith(Entity entity) {
		if (entity.getType() == EntityType.PLAYER) {
			EntityLiving player = (EntityLiving)entity;
			if (inventory.isEmpty()) {
				DialogueManager.getManager().displayMessage("The chest is empty");
			} else if (player.getInventory().isFull()) {
				DialogueManager.getManager().displayMessage("Inventory is full");
			} else {
				DialogueManager.getManager().displayMessage("Picked up: " + inventory.toString());
				player.getInventory().transferInventoryIn(inventory);
				inventory.clearInventory();
			}
		}
	}
}
