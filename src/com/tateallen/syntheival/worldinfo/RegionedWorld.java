package com.tateallen.syntheival.worldinfo;

import java.util.ArrayList;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.helpers.Utils;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.worldinfo.generators.Location;

public class RegionedWorld implements GameWorld {
	
	private Region[][] regionMap;
	private int regionSize;
	private int regionWidth;
	private int regionHeight;
	private ArrayList<Entity> entities;
	
	public RegionedWorld(Region[][] regionMap, int regionSize) {
		this.regionMap = regionMap;
		this.regionSize = regionSize;
		this.regionWidth = regionMap.length;
		this.regionHeight = regionMap[0].length;
	}

	@Override
	public Tile[][] getMapViewport(int xCenter, int yCenter, int viewWidth, int viewHeight) {
		Tile[][] viewport = new Tile[viewWidth][viewHeight];
		int xIndex = 0;
		int yIndex = 0;
		for (int x = -viewWidth/2 ; x < viewWidth/2; x++) {
			for (int y = -viewHeight/2; y < viewHeight/2; y++) {
				int globalX = xCenter+x;
				int globalY = yCenter+y;				
				if (!Utils.inBounds(globalX, globalY, getWidth(), getHeight()))
					continue;
				
				int regionX = (int)Math.floor(globalX/regionSize);
				int regionY = (int)Math.floor(globalY/regionSize);
								
				Region r = regionMap[regionX][regionY];
				
				int globalRegionX = regionX * regionSize;
				int globalRegionY = regionY * regionSize;
								
				viewport[xIndex][yIndex] = r.getTileAt(globalX-globalRegionX, globalY-globalRegionY);
				yIndex++;
			}
			
			yIndex = 0;
			xIndex++;
		}
		
		return viewport;
	}

	@Override
	public Tile getTileAt(int x, int y) {
		if (!Utils.inBounds(x, y, getWidth(), getHeight()))
			return null;
		
		int regionX = (int)Math.floor(x/regionSize);
		int regionY = (int)Math.floor(y/regionSize);
		int globalRegionX = regionX * regionSize;
		int globalRegionY = regionY * regionSize;
		
		return regionMap[regionX][regionY].getTileAt(x-globalRegionX, y-globalRegionY);
	}

	@Override
	public boolean canMoveTo(Entity entity, int x, int y) {
		if (!Utils.inBounds(x, y, getWidth(), getHeight()))
			return false;
		
		int regionX = (int)Math.floor(x/regionSize);
		int regionY = (int)Math.floor(y/regionSize);
		int globalRegionX = regionX * regionSize;
		int globalRegionY = regionY * regionSize;
		
		Tile t = regionMap[regionX][regionY].getTileAt(x-globalRegionX, y-globalRegionY);
		
		return t.isPassable();
	}

	@Override
	public boolean canMoveBy(Entity entity, int dx, int dy) {
		return canMoveTo(entity, dx+entity.getX(), dy+entity.getY());
	}

	@Override
	public int getWidth() {
		return regionWidth*regionSize;
	}

	@Override
	public int getHeight() {
		return regionHeight*regionSize;
	}

	@Override
	public Location getPlayerSpawnLocation() {
		return null;
	}

	@Override
	public String getName() {
		return "Regioned World";
	}

	@Override
	public void addEntity(Entity entity) {		
		int regionX = (int)Math.floor(entity.getX()/regionSize);
		int regionY = (int)Math.floor(entity.getY()/regionSize);
		int globalRegionX = regionX * regionSize;
		int globalRegionY = regionY * regionSize;
		
		regionMap[regionX][regionY].getTileAt(entity.getX()-globalRegionX, entity.getY()-globalRegionY).attachEntity(entity);
		entities.add(entity);
	}

	@Override
	public void detatchEntityFromTile(Entity entity) {
		int regionX = (int)Math.floor(entity.getX()/regionSize);
		int regionY = (int)Math.floor(entity.getY()/regionSize);
		int globalRegionX = regionX * regionSize;
		int globalRegionY = regionY * regionSize;
		
		regionMap[regionX][regionY].getTileAt(entity.getX()-globalRegionX, entity.getY()-globalRegionY).detachEntity();
		entities.remove(entity);
	}

	@Override
	public void entityMovedTo(Entity entity, int oldX, int oldY, int newX, int newY) {
		int regionX = (int)Math.floor(oldX/regionSize);
		int regionY = (int)Math.floor(oldY/regionSize);
		int globalRegionX = regionX * regionSize;
		int globalRegionY = regionY * regionSize;
		regionMap[regionX][regionY].getTileAt(oldX-globalRegionX, oldY-globalRegionY).detachEntity();
		
		regionX = (int)Math.floor(newX/regionSize);
		regionY = (int)Math.floor(newY/regionSize);
		globalRegionX = regionX * regionSize;
		globalRegionY = regionY * regionSize;
		regionMap[regionX][regionY].getTileAt(newX-globalRegionX, newY-globalRegionY).attachEntity(entity);
	}
}
