package com.tateallen.syntheival.worldinfo.generators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

import com.tateallen.syntheival.entities.CaveOpening;
import com.tateallen.syntheival.entities.CaveOpening.OpeningDirection;
import com.tateallen.syntheival.entities.Chest;
import com.tateallen.syntheival.helpers.Utils;
import com.tateallen.syntheival.helpers.Utils.Direction;
import com.tateallen.syntheival.items.IronSword;
import com.tateallen.syntheival.items.WoodenSword;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.tileinfo.TileDefs;
import com.tateallen.syntheival.worldinfo.BasicWorld;
import com.tateallen.syntheival.worldinfo.GameWorld;

//TODO: Block off parts of caves and require keys to get in

public class CavesGenerator {

	public static GameWorld generateCaves(int width, int height, Direction entryDirection) {
		Tile[][] map = new Tile[width][height];		
		float[][] heightMap = PerlinNoise.generatePerlinNoise(width, height, 5);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				map[x][y] = new Tile(TileDefs.CAVE_WALL);
			}
		}

		ArrayList<Location> locs = new ArrayList<Location>();
		for (int i = 0; i < 20; i++) {
			int srx = Utils.RNG.nextInt(width);
			int sry = Utils.RNG.nextInt(height);

			locs.add(new Location(srx, sry));
		}

		for (Location l1 : locs) {
			float h1 = heightMap[l1.x][l1.y];
			for (Location l2 : locs) {
				float h2 = heightMap[l2.x][l2.y];
				connectRegions(map, heightMap, width, height, new TileNode(h1, l1.x, l1.y, null), new TileNode(h2, l2.x, l2.y, null));
			}
		}

		GameWorld world = new BasicWorld(map, new Location(locs.get(0).x, locs.get(0).y));

		Location first = null;
		for (Location loc : locs) {
			if (world.getTileAt(loc.x, loc.y).getID() == TileDefs.CAVE_FLOOR.getID()) {
				first = loc;
				break;
			}
		}
		
		CaveOpening opening = new CaveOpening(world, null, OpeningDirection.EXIT);
		opening.moveTo(first.x, first.y);
		//world.addEntity(opening);

		//addRandomChests(locs, world);

		return world;
	}

	public static BasicWorld generateCaves2(int width, int height) {
		Tile[][] tileMap = new Tile[width][height];
		float[][] heightMap = PerlinNoise.generatePerlinNoise(width, height, 9);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				tileMap[x][y] = Utils.RNG.nextBoolean() ? new Tile(TileDefs.CAVE_FLOOR) : new Tile(TileDefs.CAVE_WALL); //heightMap[x][y] < 0.55f ? new Tile(TileDefs.CAVE_WALL) : new Tile(TileDefs.CAVE_FLOOR);
			}
		}

		tileMap = smooth(tileMap, 4);

		Location playerSpawnLocation = null;
		ArrayList<Location> locs = getCavesCenters(tileMap, width, height);
		playerSpawnLocation = locs.get(0);
		
		for (Location start : locs) {
			for (Location end : locs) {
				if (tileMap[start.x][start.y].getID() == TileDefs.CAVE_WALL.getID() || 
						tileMap[end.x][end.y].getID() == TileDefs.CAVE_WALL.getID())
					continue;
				
				TileNode startNode = new TileNode(heightMap[start.x][start.y], start.x, start.y, null);
				TileNode endNode = new TileNode(heightMap[end.x][end.y], end.x, end.y, null);
				
				connectRegions(tileMap, heightMap, width, height, startNode, endNode);				
			}
		}
		
		tileMap = smooth(tileMap, 1);
		
		BasicWorld w = new BasicWorld(tileMap, playerSpawnLocation);
		for (Location loc : locs) {
			Chest chest = new Chest(w, new WoodenSword(), new IronSword());
			w.addEntity(chest);
			chest.moveTo(loc.x, loc.y);
		}
		
		//MapRenderer.renderWorldToFile(w, "/Users/tateallen/Desktop/MAPLEL.png");
		
		return w;
	}
	
	private static Tile[][] smooth(Tile[][] tileMap, int times) {
		final int width = tileMap.length;
		final int height = tileMap[0].length;
		Tile[][] tileMapCopy = new Tile[width][height];
		for (int time = 0; time < 4; time++) {
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					int floors = 0;
					int rocks = 0;

					for (int ox = -1; ox < 2; ox++) {
						for (int oy = -1; oy < 2; oy++) {
							if (x + ox < 0 || x + ox >= width || y + oy < 0
									|| y + oy >= height)
								continue;

							if (tileMap[x + ox][y + oy].getID() == TileDefs.CAVE_FLOOR.getID())
								floors++;
							else
								rocks++;
						}
					}

					tileMapCopy[x][y] = floors >= rocks ? new Tile(TileDefs.CAVE_FLOOR) : new Tile(TileDefs.CAVE_WALL);
				}
			}

			tileMap = tileMapCopy;
		}
		
		return tileMap;
	}

	private static ArrayList<Location> getCavesCenters(Tile[][] tileMap, int width, int height) {
		ArrayList<Location> locations = new ArrayList<Location>();
		ArrayList<Location> usedLocations = new ArrayList<Location>();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (containsLocation(usedLocations, new Location(x, y)))
					continue;

				Rectangle r = new Rectangle(x, y, 1, 1);
				floodFillCave(tileMap, x, y, width, height, usedLocations, r);
				if (!r.isEqualTo(new Rectangle(x, y, 1, 1))) {
					locations.add(new Location(r.x + r.width/2, r.y + r.height/2));
				} else
					tileMap[x][y] = new Tile(TileDefs.CAVE_WALL);
			}
		}

		return locations;
	}

	private static void floodFillCave(Tile[][] tileMap, int x, int y, int width, int height, ArrayList<Location> usedLocations, Rectangle aabb) {
		if (!Utils.inBounds(x, y, width, height) || tileMap[x][y].getID() == TileDefs.CAVE_WALL.getID()) {
			return;
		} else {
			if (x < aabb.x) {
				aabb.x = x;
				aabb.width++;
			} else if (Math.abs(x - aabb.x) > aabb.width) {
				aabb.width++;
			}

			if (y < aabb.y) {
				aabb.y = y;
				aabb.height++;
			} else if (Math.abs(y - aabb.y) > aabb.height) {
				aabb.height++;
			}

			usedLocations.add(new Location(x, y));
		}

		for (int ox = -1; ox <= 1; ox++) {
			for (int oy = -1; oy <= 1; oy++) {
				if (ox == 0 && oy == 0)
					continue;
				if (!Utils.inBounds(x+ox, y+oy, width, height))
					continue;
				if (containsLocation(usedLocations, new Location(x+ox, y+oy)))
					continue;

				floodFillCave(tileMap, x+ox, y+oy, width, height, usedLocations, aabb);
			}
		}
	}

	private static boolean containsLocation(ArrayList<Location> locs, Location location) {
		for (Location loc : locs) {
			if (loc.isEqualTo(location.x, location.y))
				return true;
		}

		return false;
	}

	public static void connectRegions(Tile[][] map, float[][] heights, int width, int height, TileNode startNode, TileNode endNode) {
		PriorityQueue<TileNode> nodes = new PriorityQueue<TileNode>(new TileNodeComparator(endNode));
		nodes.add(startNode);
		TileNode deq = nodes.remove();
		while (deq.x != endNode.x && deq.y != endNode.y) {
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					if (x == 0 && y == 0)
						continue;
					if (Math.abs(x) + Math.abs(y) == 2)
						continue;

					int absX = x+deq.x;
					int absY = y+deq.y;
					if (Utils.inBounds(absX, absY, width, height))
						nodes.add(new TileNode(heights[absX][absY], absX, absY, deq));
				}
			}

			deq = nodes.remove();
		}

		while (deq != null) {
			map[deq.x][deq.y] = new Tile(TileDefs.CAVE_FLOOR);
			deq = deq.parent;
		}
	}

	private static void addRandomChests(ArrayList<Location> interestPoints, GameWorld world) {
		for (Location loc : interestPoints) {
			if (loc == interestPoints.get(0))
				continue;

			if (Utils.RNG.nextInt(4) == 0) {
				Chest chest = new Chest(world);
				chest.moveTo(loc.x, loc.y);
				//world.addEntity(chest);
			}
		}
	}

	private static class TileNode {

		public float height;
		public int x;
		public int y;
		public TileNode parent;

		public TileNode(float height, int x, int y, TileNode parent) {
			this.height = height;
			this.x = x;
			this.y = y;
			this.parent = parent;
		}
	}

	private static class TileNodeComparator implements Comparator<TileNode> {

		private TileNode destinationNode;

		public TileNodeComparator(TileNode destinationNode) {
			this.destinationNode = destinationNode;
		}

		public float weight(TileNode o) {
			int manX = Math.abs(destinationNode.x - o.x);
			int manY = Math.abs(destinationNode.y - o.y);

			return manX + manY + o.height*o.height;
		}

		@Override
		public int compare(TileNode o1, TileNode o2) {
			float w1 = weight(o1);
			float w2 = weight(o2);
			if (w1 == w2)
				return 0;
			else if (w1 > w2)
				return 1;

			return -1;
		}
	}
}
