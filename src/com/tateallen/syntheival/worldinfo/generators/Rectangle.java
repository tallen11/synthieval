package com.tateallen.syntheival.worldinfo.generators;

public class Rectangle {
	
	public int x;
	public int y;
	public int width;
	public int height;

	public Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public boolean isEqualTo(Rectangle rect) {
		return rect.x == x && rect.y == y && rect.width == width && rect.height == height;
	}
}
