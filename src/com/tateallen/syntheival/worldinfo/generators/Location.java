package com.tateallen.syntheival.worldinfo.generators;

public class Location {
	
	public int x;
	public int y;
	
	public Location(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean isEqualTo(int x, int y) {
		return this.x == x && this.y == y;
	}
	
	@Override
	public String toString() {
		return "("+x+","+y+")";
	}
}
