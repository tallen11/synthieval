package com.tateallen.syntheival.worldinfo.generators;

import java.util.ArrayList;
import java.util.Iterator;

public class Ray implements Iterable<Location> {
	
	private ArrayList<Location> points;
 
    public Ray(int x0, int y0, int x1, int y1) {
        points = new ArrayList<Location>();
     
        int dx = Math.abs(x1-x0);
        int dy = Math.abs(y1-y0);
     
        int sx = x0 < x1 ? 1 : -1;
        int sy = y0 < y1 ? 1 : -1;
        int err = dx-dy;
     
        while (true) {
            points.add(new Location(x0, y0));
         
            if (x0 == x1 && y0 == y1)
                break;
         
            int e2 = err * 2;
            if (e2 > -dx) {
                err -= dy;
                x0 += sx;
            }
            
            if (e2 < dx){
                err += dx;
                y0 += sy;
            }
        }
    }
    
    public ArrayList<Location> getLocations() {
    	return points;
    }

	@Override
	public Iterator<Location> iterator() {
		return points.iterator();
	}
}
