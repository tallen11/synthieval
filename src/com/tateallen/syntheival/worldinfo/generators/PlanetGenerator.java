package com.tateallen.syntheival.worldinfo.generators;

import com.tateallen.syntheival.helpers.Utils;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.tileinfo.TileDefs;
import com.tateallen.syntheival.tileinfo.TileReference;
import com.tateallen.syntheival.worldinfo.BasicWorld;
import com.tateallen.syntheival.worldinfo.GameWorld;
import com.tateallen.syntheival.worldinfo.Region;
import com.tateallen.syntheival.worldinfo.RegionedWorld;

public class PlanetGenerator {
	
	private static final int REGION_SIZE = 16;
	private static final int ELEVATION_MIN = -50;
	private static final int ELEVATION_MAX = 50;
	private static final int TEMPERATURE_MIN = -30;
	private static final int TEMPERATURE_MAX = 120;

	public static GameWorld generatePlanetscape(int width, int height) {
		TileDefs.getTileDefs().popululateMap();
		Tile[][] map = new Tile[width][height];
		
		int regionSize = 8;
		byte[][] detailMap = PerlinNoise.generateHeightMap2D(width*regionSize, height*regionSize, ELEVATION_MIN, ELEVATION_MAX, 10);
		byte[][] heightMap = new byte[width][height];
		
		
		int ldX = 0;
		int ldY = 0;
		for (int x = 0; x < width*regionSize; x += regionSize) {
			for (int y = 0; y < height*regionSize; y += regionSize) {
				
				int avg = 0;
				for (int ox = 0 ; ox < regionSize; ox++) {
					for (int oy = 0; oy < regionSize; oy++)
						avg += detailMap[x+ox][y+oy];
				}
				
				heightMap[ldX][ldY] = (byte)(avg / (regionSize*regionSize));
				ldY++;
			}
			
			ldY = 0;
			ldX++;
		}		
		
		int[] latitudeMap = generateLatitudeMap(height);
		byte[][] temperatureMap = generateTemperatureMap(heightMap, latitudeMap);
		float[][] rainfallMap = PerlinNoise.generatePerlinNoise(width, height, 6);

		for (int x = 0 ; x < width; x++) {
			for (int y = 0; y < height; y++) {
				byte h = heightMap[x][y];
				byte t = temperatureMap[x][y];
				int r = (int)(rainfallMap[x][y] * 100.0f);

				if (h < 0) {
					map[x][y] = new Tile(TileDefs.WATER);
				} else {
					int f = 0;
					for (int i = 0; i < TileDefs.ALL_TILES.length; i++) {
						TileReference ref = TileDefs.ALL_TILES[i];
						if (ref.getElevationRange().inRange(h) && ref.getTemperatureRange().inRange(t) && ref.getRainfallRange().inRange(r)) {
							map[x][y] = new Tile(TileDefs.getTileDefs().convertTile(ref));
							f++;
						}	
					}

					if (f == 0)
						map[x][y] = new Tile(TileDefs.getTileDefs().convertTile(TileDefs.PLAINS));
				}
			}
		}
		
		return new BasicWorld(map, null);
	}
	
	public static GameWorld generateTestRegionedWorld(int width, int height) {
		TileDefs.getTileDefs().popululateMap();
				
		byte[][] detailMap = PerlinNoise.generateHeightMap2D(width*REGION_SIZE, height*REGION_SIZE, -50, 50, 10);
		int[] latitudeMap = generateLatitudeMap(height*REGION_SIZE);
		byte[][] temperatureMap = generateTemperatureMap(detailMap, latitudeMap);
		float[][] rainfallMap = PerlinNoise.generatePerlinNoise(width*REGION_SIZE, height*REGION_SIZE, 6);
		
		Region[][] regionMap = new Region[width][height];
		
		for (int x = 0 ; x < width; x++) {
			for (int y = 0; y < height; y++) {
				
				Tile[][] regionTiles = new Tile[REGION_SIZE][REGION_SIZE];
				for (int rx = 0 ; rx < REGION_SIZE; rx++) {
					for (int ry = 0; ry < REGION_SIZE; ry++) {						
						byte h = detailMap[rx+REGION_SIZE*x][ry+REGION_SIZE*y];
						byte t = temperatureMap[rx+REGION_SIZE*x][ry+REGION_SIZE*y];
						int r = (int)(rainfallMap[rx+REGION_SIZE*x][ry+REGION_SIZE*y] * 100.0f);
						
						if (h < -1) {
							regionTiles[rx][ry] = new Tile(TileDefs.WATER);
						} else if (h == -1) {
							//System.out.println("BEACH!");
							regionTiles[rx][ry] = new Tile(TileDefs.SAND);
						} else if (h > -1) {
							int f = 0;
							for (int i = 0; i < TileDefs.ALL_TILES.length; i++) {
								TileReference ref = TileDefs.ALL_TILES[i];
								if (ref.getElevationRange().inRange(h) && ref.getTemperatureRange().inRange(t) && ref.getRainfallRange().inRange(r)) {
									regionTiles[rx][ry] = new Tile(TileDefs.getTileDefs().convertTile(ref));
									f++;
								}	
							}
							
							if (f == 0)
								regionTiles[rx][ry] = new Tile(TileDefs.getTileDefs().convertTile(TileDefs.PLAINS));
						}
					}
				}
				
				regionMap[x][y] = new Region(regionTiles);
			}
		}
		
		return new RegionedWorld(regionMap, REGION_SIZE);
	}

	private static int[] generateLatitudeMap(int height) {
		int latMap[] = new int[height];
		for (int i = 0; i < height; i++) {
			if (i <= height/2)
				latMap[i] = Utils.convertToRange(i, 0, height, 0, 500);
			else {
				latMap[i] = Utils.convertToRange(height - i, 0, height, 0, 500);
			}
		}

		return latMap;
	}

	private static byte[][] generateTemperatureMap(byte[][] heightMap, int[] latitudeMap) {
		int width = heightMap.length;
		int height = heightMap[0].length;

		byte[][] tempMap = new byte[width][height];
		for (int x = 0 ; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int base = latitudeMap[y] - heightMap[x][y];
				tempMap[x][y] = (byte)Utils.convertToRange(base, -ELEVATION_MAX, 250-ELEVATION_MIN, TEMPERATURE_MIN, TEMPERATURE_MAX);
			}
		}

		return tempMap;
	}
}
