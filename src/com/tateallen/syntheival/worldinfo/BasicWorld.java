package com.tateallen.syntheival.worldinfo;

import java.util.ArrayList;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.helpers.Utils;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.worldinfo.generators.Location;

public class BasicWorld implements GameWorld {
	
	private Tile[][] tileMap;
	private int width;
	private int height;
	private Location playerSpawnLocation;
	private ArrayList<Entity> entities;
	
	public BasicWorld(Tile[][] tileMap, Location playerSpawnLocation) {
		this.tileMap = tileMap;
		this.width = tileMap.length;
		this.height = tileMap[0].length;
		this.playerSpawnLocation = playerSpawnLocation;
		this.entities = new ArrayList<Entity>();
	}

	@Override
	public Tile[][] getMapViewport(int xCenter, int yCenter, int viewWidth, int viewHeight) {
		Tile viewport[][] = new Tile[viewWidth][viewHeight];
		int xIndex = 0;
		int yIndex = 0;
		int halfWidth = viewWidth / 2;
		int halfHeight = viewHeight / 2;
		
		for (int x = xCenter-halfWidth; x < xCenter+halfWidth; x++) {
			for (int y = yCenter-halfHeight; y < yCenter+halfHeight; y++) {
				viewport[xIndex][yIndex] = getTileAt(x, y);
				yIndex++;
			}
			
			yIndex = 0;
			xIndex++;
		}
		
		return viewport;
	}

	@Override
	public Tile getTileAt(int x, int y) {
		if (!Utils.inBounds(x, y, getWidth(), getHeight()))
			return null;
		
		return tileMap[x][y];
	}

	@Override
	public boolean canMoveTo(Entity entity, int x, int y) {
		if (!Utils.inBounds(x, y, getWidth(), getHeight()))
			return false;
		
		if (tileMap[x][y].hasEntity()) {
			tileMap[x][y].getEntity().interactedWith(entity);
			return false;
		}
		
		return tileMap[x][y].isPassable();
	}

	@Override
	public boolean canMoveBy(Entity entity, int dx, int dy) {
		return canMoveTo(entity, dx+entity.getX(), dy+entity.getY());
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public Location getPlayerSpawnLocation() {
		return playerSpawnLocation;
	}

	@Override
	public String getName() {
		return "Caves";
	}

	@Override
	public void addEntity(Entity entity) {
		entities.add(entity);
		tileMap[entity.getX()][entity.getY()].attachEntity(entity);
	}

	@Override
	public void detatchEntityFromTile(Entity entity) {
		entities.remove(entity);
		tileMap[entity.getX()][entity.getY()].detachEntity();
	}

	@Override
	public void entityMovedTo(Entity entity, int oldX, int oldY, int newX, int newY) {
		tileMap[oldX][oldY].detachEntity();
		tileMap[newX][newY].attachEntity(entity);
	}
}
