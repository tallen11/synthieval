package com.tateallen.syntheival.worldinfo;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.worldinfo.generators.Location;

public interface GameWorld {
	public Tile[][] getMapViewport(int xCenter, int yCenter, int viewWidth, int viewHeight);
	public Tile getTileAt(int x, int y);
	public boolean canMoveTo(Entity entity, int x, int y);
	public boolean canMoveBy(Entity entity, int dx, int dy);
	public int getWidth();
	public int getHeight();
	public Location getPlayerSpawnLocation();
	public String getName();
	public void addEntity(Entity entity);
	public void detatchEntityFromTile(Entity entity);
	public void entityMovedTo(Entity entity, int oldX, int oldY, int newX, int newY);
}
