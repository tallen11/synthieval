package com.tateallen.syntheival.worldinfo;
 
import com.tateallen.syntheival.tileinfo.Tile;

public class Region {

	private Tile[][] tileMap;
	
	public Region(Tile[][] tileMap) {
		this.tileMap = tileMap;
	}
	
	public Tile getTileAt(int x, int y) {
		return tileMap[x][y];
	}
}
