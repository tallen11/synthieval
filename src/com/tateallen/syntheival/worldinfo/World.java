package com.tateallen.syntheival.worldinfo;

import java.util.ArrayList;

import com.tateallen.syntheival.entityinfo.Entity;
import com.tateallen.syntheival.helpers.Utils;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.worldinfo.generators.Location;

public class World {
	
	private Tile[][] tileMap;
	private int width;
	private int height;
	private ArrayList<Entity> entities;
	private Location playerSpawnLocation;
	
	public World(Tile[][] tileMap) {
		this.tileMap = tileMap;
		this.width = tileMap.length;
		this.height = tileMap[0].length;
		this.entities = new ArrayList<Entity>();
		this.playerSpawnLocation = null;
	}
	
	public World(Tile[][] tileMap, Location playerSpawnLocation) {
		this.tileMap = tileMap;
		this.width = tileMap.length;
		this.height = tileMap[0].length;
		this.entities = new ArrayList<Entity>();
		this.playerSpawnLocation = playerSpawnLocation;
	}
	
	public Tile[][] getMapViewport(int xCenter, int yCenter, int viewWidth, int viewHeight) {
		Tile viewport[][] = new Tile[80][24];
		int xIndex = 0;
		int yIndex = 0;
		int halfWidth = viewWidth / 2;
		int halfHeight = viewHeight / 2;
		
		for (int x = xCenter-halfWidth; x < xCenter+halfWidth; x++) {
			for (int y = yCenter-halfHeight; y < yCenter+halfHeight; y++) {
				viewport[xIndex][yIndex] = getTileAt(x, y);
				yIndex++;
			}
			
			yIndex = 0;
			xIndex++;
		}
		
		return viewport;
	}
	
	public Tile getTileAt(int x, int y) {
		if (!Utils.inBounds(x, y, width, height))
			return null;
		
		return tileMap[x][y];
	}
	
	public boolean canMoveTo(Entity entity, int x, int y) {
		if (!Utils.inBounds(x, y, width, height))
			return false;
		
		if (tileMap[x][y].hasEntity()) {
			tileMap[x][y].getEntity().interactedWith(entity);
			return false;
		}
		
		return tileMap[x][y].isPassable();
	}
	
	public boolean canMoveBy(Entity entity, int dx, int dy) {
		return canMoveTo(entity, dx+entity.getX(), dy+entity.getY());
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public Location getPlayerSpawnLocation() {
		return playerSpawnLocation;
	}
	
	public void addEntity(Entity entity) {
		entities.add(entity);
		tileMap[entity.getX()][entity.getY()].attachEntity(entity);
	}
	
	public void detatchEntityFromTile(Entity entity) {
		tileMap[entity.getX()][entity.getY()].detachEntity();
	}
	
	public void update() {
		for (Entity entity : entities)
			entity.update();
	}
}
