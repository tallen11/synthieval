package com.tateallen.syntheival.entityinfo;

import java.awt.Color;

import com.tateallen.syntheival.iteminfo.Inventory;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.worldinfo.GameWorld;
import com.tateallen.syntheival.worldinfo.generators.Location;
import com.tateallen.syntheival.worldinfo.generators.Ray;

public class EntityLiving extends Entity {
	
	private int health;
	private Inventory inventory;
	private FOV fov;

	public EntityLiving(GameWorld world, char glyph, Color color, int health, EntityType type, FOV fov) {
		super(world, glyph, color, type);
		this.health = 0;
		inventory = new Inventory(10);
		this.fov = fov;
	}
	
	public int getHealth() {
		return health;
	}
	
	public Inventory getInventory() {
		return inventory;
	}
	
	public void heal(int hp) {
		health += hp;
	}
	
	public void damage(int hp) {
		health -= hp;
	}
	
	public FOV getFOV() {
		return fov;
	}
	
	public boolean canSee(int x, int y) {
		for (Location loc : new Ray(getX(), getY(), x, y)) {
			if (!getWorld().getTileAt(loc.x, loc.y).isPassable())
				return false;
		}
		
		return true;
	}
	
	public boolean canSee(int x, int y, Tile[][] viewport) {
		Ray cast = new Ray(viewport.length/2, viewport[0].length/2, x, y);
		for (Location loc : cast) {
			if (loc.x == x && loc.y == y)
				return true;
			
			if (viewport[loc.x][loc.y] != null && !viewport[loc.x][loc.y].isPassable())
				return false;
		}
		
		return true;
	}
}