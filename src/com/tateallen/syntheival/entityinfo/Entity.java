package com.tateallen.syntheival.entityinfo;

import java.awt.Color;

import com.tateallen.syntheival.worldinfo.GameWorld;

public class Entity {
	
	public enum EntityType {
		NON_LIVING,
		PLAYER,
		PASSIVE,
		AGGRESSIVE
	}
		
	private GameWorld world;
	private char glyph;
	private Color color;
	private EntityType type;
	protected int xPos;
	protected int yPos;
	
	public Entity(GameWorld world, char glyph, Color color, EntityType type) {
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.type = type;
		this.xPos = 0;
		this.yPos = 0;
	}

	public char getGlyph() {
		return glyph;
	}

	public void setGlyph(char glyph) {
		this.glyph = glyph;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public EntityType getType() {
		return type;
	}
	
	protected GameWorld getWorld() {
		return world;
	}
	
	public int getX() {
		return xPos;
	}

	public int getY() {
		return yPos;
	}
	
	public void moveTo(int x, int y) {
		world.entityMovedTo(this, xPos, yPos, x, y);
		this.xPos = x;
		this.yPos = y;
	}
	
	public void moveBy(int dx, int dy) {
		moveTo(xPos+dx, yPos+dy);
	}

	public void update() { }
	public void interactedWith(Entity entity) { }
}
