package com.tateallen.syntheival.entityinfo;

public class FOV {

	private int viewRadius;
	
	public FOV(int viewRadius) {
		this.viewRadius = viewRadius;
	}
	
	public int getViewRadius() {
		return viewRadius;
	}
}
