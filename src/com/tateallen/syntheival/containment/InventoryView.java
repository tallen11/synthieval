package com.tateallen.syntheival.containment;

import asciiPanel.AsciiPanel;

import com.tateallen.syntheival.entityinfo.EntityLiving;
import com.tateallen.syntheival.iteminfo.Inventory;
import com.tateallen.syntheival.iteminfo.Item;

public class InventoryView extends View {

	private EntityLiving hostEntity;
	
	public InventoryView(EntityLiving hostEntity) {
		super("Inventory");
		
		this.hostEntity = hostEntity;
	}
	
	@Override
	public void draw(AsciiPanel terminal) {
		super.draw(terminal);
		
		Inventory inv = hostEntity.getInventory();
		if (inv.isEmpty()) {
			terminal.write("empty...", getInsetLineX(0), getInsetLineY(0));
		} else {
			int index = 0;
			for (Item item : inv) {
				terminal.write((index+1) + " " + item.getName(), getInsetLineX(0), getInsetLineY(index));
				index++;
			}
		}
	}
}
