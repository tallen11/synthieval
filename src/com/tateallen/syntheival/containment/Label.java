package com.tateallen.syntheival.containment;

import java.awt.Color;

import asciiPanel.AsciiPanel;

public class Label extends Drawable {
	
	private String text;
	private Color color;
	
	public Label(String text) {
		this.text = text;
		color = AsciiPanel.white;
	}

	@Override
	public void draw(AsciiPanel terminal) {
		terminal.write(getText(), getOriginX(), getOriginY(), getColor());
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
}
