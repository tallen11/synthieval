package com.tateallen.syntheival.containment;

import java.util.ArrayList;

import com.tateallen.syntheival.helpers.DialogueManager;

import asciiPanel.AsciiPanel;

public class DialogView extends View {
	
	private ArrayList<Label> labels;

	public DialogView() {
		super("");
		this.labels = new ArrayList<Label>();
	}
	
	public void generateLabels() {
		for (int i = 0; i < getHeight() - 2; i++) {
			Label label = new Label("");
			label.setOriginX(getOriginX() + 1);
			label.setOriginY(getOriginY() + i + 1);
			labels.add(label);
			DialogueManager.getManager().registerLabel(label);
		}
	}
	
	@Override
	public void draw(AsciiPanel terminal) {
		super.draw(terminal);
		
		for (Label label : labels) {
			label.draw(terminal);
		}
	}
}
