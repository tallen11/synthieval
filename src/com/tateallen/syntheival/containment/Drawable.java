package com.tateallen.syntheival.containment;

import asciiPanel.AsciiPanel;

public abstract class Drawable {
	
	private int originX;
	private int originY;
	
	public Drawable() {
		this.originX = 0;
		this.originY = 0;
	}
	
	public Drawable(int originX, int originY) {
		this.originX = originY;
		this.originY = originX;
	}
	
	public abstract void draw(AsciiPanel terminal);
	
	public int getOriginX() {
		return originX;
	}
	
	public int getOriginY() {
		return originY;
	}
	
	public void setOriginY(int originX) {
		this.originY = originX;
	}
	
	public void setOriginX(int originY) {
		this.originX = originY;
	}
}
