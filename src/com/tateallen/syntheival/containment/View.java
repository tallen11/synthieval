package com.tateallen.syntheival.containment;

import java.awt.Color;

import asciiPanel.AsciiPanel;

public class View extends Drawable {
	
	private String viewTitle;
	private int width;
	private int height;
	private boolean border;
	private Color titleColor;
	private Color borderColor;
	
	public View(String viewTitle) {
		this.viewTitle = viewTitle;
		this.width = 0;
		this.height = 0;
		this.border = true;
		this.titleColor = AsciiPanel.white;
		this.borderColor = AsciiPanel.white;
	}
	
	@Override
	public void draw(AsciiPanel terminal) {
		if (isBorder()) {
			drawBorder(terminal);
		}
	}
	
	private void drawBorder(AsciiPanel terminal) {
		final int originX = getOriginX();
		final int originY = getOriginY();
		
		terminal.write((char)201, originX, originY, borderColor);					//Top left
		terminal.write((char)187, originX+width, originY, borderColor);				//Top right
		terminal.write((char)200, originX, originY+height-1, borderColor);			//Bottom left
		terminal.write((char)188, originX+width, originY+height-1, borderColor);	//Bottom right
		
		for (int i = 1; i < width; i++) {
			terminal.write((char)205, originX+i, originY, borderColor);
			terminal.write((char)205, originX+i, originY+height-1, borderColor);
		}
		
		for (int i = 1; i < height-1; i++) {
			terminal.write((char)186, originX, originY+i, borderColor);
			terminal.write((char)186, originX+width, originY+i, borderColor);
		}
		
		if (getViewTitle().length() > 0) {
			terminal.write(" " + getViewTitle() + " ", width/2+originX - getViewTitle().length()/2 - 1, originY, titleColor);
		}
	}
	
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isBorder() {
		return border;
	}
	
	public void setTitleColor(Color color) {
		this.titleColor = color;
	}
	
	public Color getTitleColor() {
		return titleColor;
	}
	
	public void setBorderColor(Color color) {
		this.borderColor = color;
	}
	
	public Color getBorderColor() {
		return borderColor;
	}

	public void setBorder(boolean border) {
		this.border = border;
	}
	
	public String getViewTitle() {
		return viewTitle;
	}
	
	public void setViewTitle(String viewTitle) {
		this.viewTitle = viewTitle;
	}
	
	public int getInsetLineX(int index) {
		return index + getOriginX() + 1;
	}
	
	public int getInsetLineY(int index) {
		return index + getOriginY() + 1;
	}
}
