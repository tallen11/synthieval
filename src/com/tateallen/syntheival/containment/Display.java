package com.tateallen.syntheival.containment;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import asciiPanel.AsciiPanel;

public abstract class Display {
	
	private ArrayList<Drawable> drawables;
	
	public Display() {
		drawables = new ArrayList<Drawable>();
	}
	
	public void addNode(Drawable node) {
		drawables.add(node);
	}
	
	public void removeNode(Drawable node) {
		drawables.remove(node);
	}
	
	public void draw(AsciiPanel terminal) {
		for (Drawable drawable : drawables) {
			drawable.draw(terminal);
		}
	}
	
	public abstract void keyPressed(KeyEvent event);
	public abstract void update();
}
