package com.tateallen.syntheival.containment;

import java.awt.Color;

import asciiPanel.AsciiPanel;

import com.tateallen.syntheival.entityinfo.EntityLiving;
import com.tateallen.syntheival.helpers.Utils;
import com.tateallen.syntheival.tileinfo.Tile;
import com.tateallen.syntheival.worldinfo.GameWorld;

public class GameView extends View {
	
	private GameWorld world;
	private EntityLiving centralEntity;

	public GameView(GameWorld world, EntityLiving centralEntity) {
		super(world.getName());
		this.world = world;
		this.centralEntity = centralEntity;
	}
	
	@Override
	public void draw(AsciiPanel terminal) {
		Tile[][] viewport = world.getMapViewport(centralEntity.getX(), centralEntity.getY(), getWidth(), getHeight());

		int localPlayerX = (getWidth() / 2);
		int localPlayerY = (getHeight() / 2);

		for (int x = 0; x < getWidth(); x++) {
			for (int y = 0; y < getHeight(); y++) {
				int viewDistance = (int)Utils.distance(x, y, localPlayerX, localPlayerY); //= manX + manY;

				Tile tile = viewport[x][y];
				char glyph = tile == null ? ' ' : tile.getGlyph();
				Color tileColor = tile == null ? Color.black : tile.getColor();
				if (tile != null && tile.hasEntity()) {
					glyph = tile.getEntity().getGlyph();
					tileColor = tile.getEntity().getColor();
				}

				if (tile != null && viewDistance <= centralEntity.getFOV().getViewRadius() && centralEntity.canSee(x, y, viewport)) {
					tile.visit();										//In our immediate FOV
				} else if (tile != null && tile.isVisited()) {
					tileColor = tileColor.darker().darker().darker();	//Outside FOV, but we've visited it before
				} else {
					tileColor = Color.black;							//NULL or undiscovered
				}

				terminal.write(glyph, x+getOriginX(), y+getOriginY(), tileColor);
			}
		}
		
		super.draw(terminal);
	}
}
