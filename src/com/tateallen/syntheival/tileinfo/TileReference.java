package com.tateallen.syntheival.tileinfo;

import java.awt.Color;

import com.tateallen.syntheival.helpers.Range;
import com.tateallen.syntheival.helpers.Utils;

public class TileReference {
	
	//TODO: Split this into multiple classes!!! 
	
	private char[] glyphs;
	
	private char[] detailGlyphs;
	private int detailChance;	//Chance is 1 / detailChance so higher = less likely
	private boolean detailPassable;
	private Color detailColor;
	
	private Color color;
	private boolean passable;
	private int ID;
	private Range elevationRange;
	private Range temperatureRange;
	private Range rainfallRange;
	
	public TileReference(char[] glyphs, Color color, boolean passable, int ID, Range elevRange, Range tempRange, Range rainRange) {
		this.glyphs = glyphs;
		this.color = color;
		this.passable = passable;
		this.ID = ID;
		this.elevationRange = elevRange;
		this.temperatureRange = tempRange;
		this.rainfallRange = rainRange;
		
		this.detailGlyphs = null;
		this.detailChance = 0;
		this.detailColor = null;
		this.detailPassable = passable;
	}
	
	public TileReference(char[] glyphs, Color color, boolean passable, int ID) {
		this.glyphs = glyphs;
		this.color = color;
		this.passable = passable;
		this.ID = ID;
		this.elevationRange = null;
		this.temperatureRange = null;
		this.rainfallRange = null;
		
		this.detailGlyphs = null;
		this.detailChance = 0;
		this.detailColor = null;
		this.detailPassable = passable;
	}
	
	public TileReference(char[] glyphs, char[] detailGlyphs, Color color, Color detailColor, boolean passable, boolean detailPassable, int detailChance, int ID) {
		this.glyphs = glyphs;
		this.color = color;
		this.passable = passable;
		this.ID = ID;
		this.elevationRange = null;
		this.temperatureRange = null;
		this.rainfallRange = null;
		
		this.detailGlyphs = detailGlyphs;
		this.detailChance = detailChance;
		this.detailColor = detailColor;
		this.detailPassable = detailPassable;
	}
	
	public char getGlyph() {
		return glyphs[Utils.RNG.nextInt(glyphs.length)];
	}
	
	public Color getColor() {
		return color;
	}
	
	public boolean isPassable() {
		return passable;
	}
	
	public int getID() {
		return ID;
	}

	public Range getElevationRange() {
		return elevationRange;
	}

	public Range getTemperatureRange() {
		return temperatureRange;
	}

	public Range getRainfallRange() {
		return rainfallRange;
	}

	public char getDetailGlyph() {
		return detailGlyphs[Utils.RNG.nextInt(detailGlyphs.length)];
	}

	public int getDetailChance() {
		return detailChance;
	}

	public boolean isDetailPassable() {
		return detailPassable;
	}

	public Color getDetailColor() {
		return detailColor;
	}
	
	public boolean shouldDetail() {
		if (detailChance > 0)
			return Utils.RNG.nextInt(detailChance) == 0;
		
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
				
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		TileReference other = (TileReference) obj;
		if (ID != other.ID)
			return false;
		
		return true;
	}
	
	/*
	 * @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj.getClass() != this.getClass())
			return false;
		
		System.out.println("Comparison");
		
		TileReference other = (TileReference)obj;
		return getID() == other.getID();
	}
	 */
}
