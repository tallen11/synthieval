package com.tateallen.syntheival.tileinfo;

import java.awt.Color;

import com.tateallen.syntheival.entityinfo.Entity;

public class Tile {
	
	private int ID;
	private char glyph;
	private Color color;
	private boolean passable;
	private boolean visited;
	private Entity attachedEntity;
	
	public Tile(TileReference ref) {
		boolean detail = ref.shouldDetail();
		this.glyph = detail ? ref.getDetailGlyph() : ref.getGlyph();
		this.color = detail ? ref.getDetailColor() : ref.getColor();
		this.passable = detail ? ref.isDetailPassable() : ref.isPassable();
		this.ID = ref.getID();
		this.visited = false;
		this.attachedEntity = null;
	}
	
	public int getID() {
		return ID;
	}
	
	public char getGlyph() {
		return glyph;
	}
	
	public Color getColor() {
		return color;
	}
	
	public boolean isPassable() {
		return passable;
	}
	
	public boolean equals(Tile t) {
		return t.getID() == getID();
	}
	
	public boolean isVisited() {
		return visited;
	}
	
	public void visit() {
		visited = true;
	}
	
	public void unvisit() {
		visited = false;
	}
	
	public void attachEntity(Entity entity) {
		attachedEntity = entity;
	}
	
	public void detachEntity() {
		attachedEntity = null;
	}
	
	public boolean hasEntity() {
		return attachedEntity != null;
	}
	
	public Entity getEntity() {
		return attachedEntity;
	}
}
