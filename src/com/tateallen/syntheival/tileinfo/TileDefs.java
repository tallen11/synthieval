package com.tateallen.syntheival.tileinfo;

import java.awt.Color;
import java.util.HashMap;

import asciiPanel.AsciiPanel;

import com.tateallen.syntheival.helpers.Range;

public class TileDefs {
	
	private static final char[] PLAINS_GLYPHS = {239, 234};
	private static final char[] DESERT_GLYPHS = {227, 239};
	private static final char[] FOREST_GLYPHS = {6, 23, 24, 5};
	private static final char[] RAINFOREST_GLYPHS = {157, 234};
	private static final char[] SWAMP_GLYPHS = {23, 21, 244};
	private static final char[] MOUNTAIN_GLYPHS = {30};
	private static final char[] PEAK_GLYPHS = {127, 30};
	private static final char[] GLACIER_GLYPHS = {176, 177, 178};
	private static final char[] WATER_GLYPHS = {'~', 247};
	private static final char[] RIVER_GLYPHS = {19};
	
	public static final TileReference DESERT = new TileReference(DESERT_GLYPHS, new Color(232, 201, 0), true, 0, new Range(0, 20), new Range(80, 120), new Range(0, 30));
	public static final TileReference FOREST = new TileReference(FOREST_GLYPHS, new Color(50, 156, 53), true, 1, new Range(0, 25), new Range(10, 90), new Range(30, 100));
	public static final TileReference SWAMP = new TileReference(SWAMP_GLYPHS, new Color(112, 133, 70), true, 2, new Range(0, 20), new Range(70, 100), new Range(60, 100));
	public static final TileReference MOUNTAIN = new TileReference(MOUNTAIN_GLYPHS, new Color(94, 94, 94), false, 3, new Range(20, 30), new Range(0, 120), new Range(0, 100));
	public static final TileReference PEAK = new TileReference(PEAK_GLYPHS, Color.white, true, 4, new Range(30, 50), new Range(0, 120), new Range(0, 100));
	public static final TileReference GLACIER = new TileReference(GLACIER_GLYPHS, new Color(42, 220, 247), true, 5, new Range(-50, 50), new Range(-30, 10), new Range(0, 100));
	public static final TileReference WATER = new TileReference(WATER_GLYPHS, new Color(32, 83, 171), true, 6, new Range(0, 0), new Range(0, 0), new Range(0, 0));
	public static final TileReference RAINFOREST = new TileReference(RAINFOREST_GLYPHS, new Color(7, 82, 0), true, 7, new Range(0, 30), new Range(80, 105), new Range(40, 100));
	public static final TileReference PLAINS = new TileReference(PLAINS_GLYPHS, new Color(147, 245, 0), true, 8, new Range(0, 40), new Range(50, 110), new Range(60, 100));
	//Experimental
	public static final TileReference RIVER = new TileReference(RIVER_GLYPHS, AsciiPanel.brightRed, true, 9, null, null ,null);
	public static final TileReference BEACH = new TileReference(DESERT_GLYPHS, new Color(232, 201, 0), true, 10, null, null, null);
	
	public static final TileReference[] ALL_TILES = {DESERT, FOREST, RAINFOREST, PLAINS, SWAMP, MOUNTAIN, PEAK, GLACIER};
	
	private static final char[] CAVE_WALL_GLYPHS = {219};
	private static final char[] CAVE_FLOOR_GLYPHS = {'.'};
	
	public static final TileReference CAVE_WALL = new TileReference(CAVE_WALL_GLYPHS, new Color(150, 150, 150), false, 100);
	public static final TileReference CAVE_FLOOR = new TileReference(CAVE_FLOOR_GLYPHS, new Color(120, 120, 120), true, 101);
	
	private static final char[] GRASS_GLYPHS = {'\'', '.', '`', '"', ','};
	private static final char[] SAND_GLYPHS = {'.', '~'};
	private static final char[] TREES_GLYPHS = {'\'', '.', '`', '"', ','};
	private static final char[] MARSH_GLYPHS = {'\'', '.', '`', '"', ','};
	private static final char[] JUNGLE_GLYPHS = {'\'', '.', '`', '"', ','};		//TODO: Change these?
	
	private static final char[] GRASS_DETAILS = {'~'};
	private static final char[] TREES_DETAILS = {6, 23, 24, 5};
	private static final char[] MARSH_DETAILS = {15, 42};
	private static final char[] JUNGLE_DETAILS = {24, 226};
	
	public static final TileReference GRASS = new TileReference(GRASS_GLYPHS, GRASS_DETAILS, AsciiPanel.green, AsciiPanel.yellow, true, true, 15, 200);
	public static final TileReference SAND = new TileReference(SAND_GLYPHS, null, AsciiPanel.yellow, null, true, true, 0, 201);
	public static final TileReference TREES = new TileReference(TREES_GLYPHS, TREES_DETAILS, new Color(50, 156, 53).darker().darker(), new Color(50, 156, 53), true, false, 10, 202);
	public static final TileReference MARSHES = new TileReference(MARSH_GLYPHS, MARSH_DETAILS, new Color(112, 133, 70), new Color(107, 0, 179), true, false, 15, 203);
	public static final TileReference JUNGLE = new TileReference(JUNGLE_GLYPHS, JUNGLE_DETAILS, new Color(7, 82, 0).darker(), new Color(7, 82, 0), true, false, 5, 204);
	
	private static TileDefs instance = null;
	private HashMap<TileReference, TileReference> conversionMap;
	private TileDefs() {}
	
	public static TileDefs getTileDefs() {
		if (instance == null)
			instance = new TileDefs();
		return instance;
	}
	
	public void popululateMap() {
		if (conversionMap != null)
			return;
				
		conversionMap = new HashMap<TileReference, TileReference>();
		conversionMap.put(PLAINS, GRASS);
		conversionMap.put(DESERT, SAND);
		conversionMap.put(FOREST, TREES);
		conversionMap.put(SWAMP, MARSHES);
		conversionMap.put(RAINFOREST, JUNGLE);
	}
	
	public TileReference convertTile(TileReference ref) {
		if	(conversionMap.containsKey(ref))	
			return conversionMap.get(ref);
		else
			return ref;
	}
}
