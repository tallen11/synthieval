package com.tateallen.syntheival;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

import asciiPanel.AsciiPanel;

import com.tateallen.syntheival.containment.Display;
import com.tateallen.syntheival.displays.DisplayHome;
import com.tateallen.syntheival.helpers.Manager;

public class Synthieval extends JFrame implements KeyListener {
	
	//TODO: Make castles with dungeons
	
	private static final long serialVersionUID = 7175968644308051155L;
	private Display activeDisplay;
	private AsciiPanel terminal;
	
	public Synthieval() {
		terminal = new AsciiPanel(80, 36);
		System.out.println(terminal.getWidthInCharacters() + ", " + terminal.getHeightInCharacters());
		this.add(terminal);
		this.pack();
		this.addKeyListener(this);
		
		Manager.getManager().setGameInstance(this);
		Manager.getManager().setScreenDimensions(terminal.getWidthInCharacters(), terminal.getHeightInCharacters());
		
		activeDisplay = new DisplayHome();
		
		this.repaint();
	}
	
	@Override
	public void repaint() {
		terminal.clear();
		activeDisplay.draw(terminal);
		super.repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		activeDisplay.keyPressed(e);
		this.repaint();
		
		//TODO: Implement updating here
		/*
		 * if (e is turn key)
		 * 		activeDisplay.update()
		 */
	}
	
	public void changeActiveDisplay(Display newDisplay) {
		activeDisplay = newDisplay;
		this.repaint();
	}
	
	@Override
	public void keyTyped(KeyEvent e) { }

	@Override
	public void keyReleased(KeyEvent e) { }
	
	public static void main(String[] args) {
		Synthieval game = new Synthieval();
		game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.setResizable(false);
		game.setVisible(true);
	}
}
